-------------------------------------------------------------------------------
-- MBC-UNLOCK - Bandai WonderSwan mapper validation
-- Written in 2019 by Alex "trap15" Marshall <trap15@raidenii.net>
--
-- To the extent possible under law, the author(s) have dedicated all copyright
-- and related and neighboring rights to this software to the public domain
-- worldwide. This software is distributed without any warranty.
-- 
-- You should have received a copy of the CC0 Public Domain Dedication along
-- with this software. If not, see
-- <http://creativecommons.org/publicdomain/zero/1.0/>
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity mbc_unlock is
  port(
    -- Cart reset (Pin 40)
    rst_n:  in  std_logic;
    -- Cart clock (Pin 47)
    clock:  in  std_logic;
    -- A19 (Pin 30) & A18 (Pin 31) & A17 (Pin 32) & A16 (Pin 33) &
    --  A3 (Pin 29) &  A2 (Pin 28) &  A1 (Pin 27) &  A0 (Pin 26)
    ioport: in  std_logic_vector(7 downto 0);
    -- Cart MBC (Pin 41)
    mbc_n:  out std_logic);
end mbc_unlock;

architecture arch of mbc_unlock is
  constant MBC_SEQUENCE_LENGTH: integer := 20;
  constant MBC_SEQUENCE_DATA: std_logic_vector(MBC_SEQUENCE_LENGTH-1 downto 0) := "11000000101000101000";
  signal mbc_shift_reg: std_logic_vector(MBC_SEQUENCE_LENGTH-1 downto 0);
  signal mbc_waiting: std_logic;
begin
  -- MBC update
  process(clock, rst_n)
  begin
    if(rst_n = '0') then -- Reset
      mbc_waiting <= '1';
      mbc_shift_reg <= MBC_SEQUENCE_DATA;
      mbc_n <= '1';
    elsif(rising_edge(clock)) then
      -- Wait for start (I/O reg = 0A5h)
      if(mbc_waiting = '1') then
        mbc_n <= '1';
        if (ioport = x"A5") then
          -- Indicated start
          mbc_waiting <= '0';
        end if;
      else
        -- Output sequence
        mbc_n <= mbc_shift_reg(MBC_SEQUENCE_LENGTH-1);
        mbc_shift_reg(MBC_SEQUENCE_LENGTH-1 downto 1) <= mbc_shift_reg(MBC_SEQUENCE_LENGTH-2 downto 0);
        -- Shift '1' back in, so we don't have to enter a different state when finished
        mbc_shift_reg(0) <= '1';
      end if;
    end if;
  end process;
end arch;
